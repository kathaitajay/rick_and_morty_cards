import { NgModule, Injectable } from '@angular/core';
import { Routes, RouterModule, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { APIresolver } from './appResolver';
import { AppcompComponent } from './appcomp/appcomp.component';
import { AboutComponent } from './about/about.component';
import { WllComponent } from './wll/wll.component';


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
    {
      path:'',
      pathMatch:'full',
      redirectTo:'app'
    },
    {
      path: 'app',
      component: AppcompComponent,
      resolve: {
        'characters':APIresolver 
      }
    },
    {
      path:'home',component:AboutComponent,
    },
    {
      path:'about',component:WllComponent,
    },
    {
    path:'main',component:AppcompComponent,
    resolve: {
      'characters':APIresolver 
    }
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
