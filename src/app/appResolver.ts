import { Injectable } from '@angular/core';
import { FetchFromApiService } from './fetch-from-api.service';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class APIresolver implements Resolve<Promise<any>> {
  constructor(private apiService: FetchFromApiService) {}

  resolve(route: ActivatedRouteSnapshot,state:RouterStateSnapshot):Promise<any>{
      //console.log('in resolver',route,state);
     return this.apiService.call("https://rickandmortyapi.com/api/character/","get").catch((err)=>{
     });
  }
}