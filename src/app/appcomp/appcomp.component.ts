import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, RouterState } from '@angular/router';
import { FetchFromApiService } from '../fetch-from-api.service';


@Component({
  selector: 'app-appcomp',
  templateUrl: './appcomp.component.html',
  styleUrls: ['./appcomp.component.css']
})
export class AppcompComponent implements OnInit {
  public characters = [];
  public info = {'next':null,
                  'prev':null,
                  'pages':null};
  status;
  gender;
  species;
  order='asec';
  name;
  nextButtonAlive=true;
  prevButtonAlive=false;
  dataIsAvailable = false;
  constructor(private acrt: ActivatedRoute, private fetchFromApi: FetchFromApiService) {
    console.log('main page');
    this.characters = acrt.snapshot.data.characters.results;
    this.info = acrt.snapshot.data.characters.info;
    if(this.order=='desc'){
      //swap code
      let temp=this.info.next;
      this.info.next=this.info.prev;
      this.info.prev=temp;
   }
    this.dataIsAvailable = true;
  }
  ngOnInit() {
  }
  ngOnDestroy() {
  }
  getNextPage() {
    if(this.info.next){
    this.fetchFromApi.call(this.info.next,'get').then((results) => {
      console.log('next called',results);
      this.prevButtonAlive=true;
      this.info=JSON.parse(JSON.stringify(results)).info;
      if(this.order=='desc'){
        //swap code
        let temp=this.info.next;
        this.info.next=this.info.prev;
        this.info.prev=temp;
     }
      this.characters=JSON.parse(JSON.stringify(results)).results;
    }).catch((err)=>{
      console.log(err);
    })
    
  }else{
    this.nextButtonAlive=false;
    alert('no next page available');
  }

  }
  getPrevPage() {
    if(this.info.prev){
    this.fetchFromApi.call(this.info.prev,'get').then((results) => {
      console.log('next called',results);
      this.nextButtonAlive=true;
      this.info=JSON.parse(JSON.stringify(results)).info;
      if(this.order=='desc'){
         //swap code
         let temp=this.info.next;
         this.info.next=this.info.prev;
         this.info.prev=temp;
      }
      this.characters=JSON.parse(JSON.stringify(results)).results;
    }).catch((err)=>{
      console.log(err);
    })
    
  }
  else{
    this.prevButtonAlive=false;
   alert('no prev page available');
  }

  }

  setFilter(){
    let filters={ 
      status:this.status,
      gender:this.gender,
      species:this.species,
      name:this.name
    }
    console.log(this.species,this.status, this.gender);
    console.log(filters);
    filters=JSON.parse(JSON.stringify(filters))
    this.fetchFromApi.setFilter(filters);
    filters=this.fetchFromApi.getFilter();
    console.log(filters);
      this.fetchFromApi.callWithFilters("https://rickandmortyapi.com/api/character/","get",filters).then(results=>{
        let parsedValues=JSON.parse(JSON.stringify(results));
        this.characters =parsedValues.results;
        this.info = parsedValues.info;
        if(this.order=='desc'){
          //swap code
          let temp=this.info.next;
          this.info.next=this.info.prev;
          this.info.prev=temp;
       }
        this.dataIsAvailable = true;
        console.log('reached setfilter');
        console.log(results);
      }).catch(err=>{
        //console.log(err);
        this.characters=[] ;
        this.info = {'next':null,
                    'prev':null,
                      'pages':null};
        this.dataIsAvailable = false;
        setTimeout(()=>{
          alert("No characters avalable");
        },1000);
      });
  }   
  clearFilters(){
    this.status=undefined;
    this.gender=undefined;
    this.species=undefined;
    this.name=undefined;
    this.setFilter();
  }
  sortOrder(){
    if(this.order=='desc'){
      if(this.info.pages){
        let filters=this.fetchFromApi.getFilter();
        filters['page']=this.info.pages;
        this.fetchFromApi.callWithFilters("https://rickandmortyapi.com/api/character/","get",filters).then((results)=>{
          let parsedValues=JSON.parse(JSON.stringify(results));
          this.characters =parsedValues.results;
          this.info = parsedValues.info;

          //swap code
          let temp=this.info.next;
          this.info.next=this.info.prev;
          this.info.prev=temp;


          this.dataIsAvailable = true;
          console.log('reached setfilter');
          console.log(results);
        }).catch((err)=>{
          this.characters=[] ;
          this.info = {'next':null,
                      'prev':null,
                        'pages':null};
          this.dataIsAvailable = false;
          setTimeout(()=>{
            alert("No characters avalable");
          },1000);
        })
      }
    }else{
      this.setFilter();
    }
  }

}
