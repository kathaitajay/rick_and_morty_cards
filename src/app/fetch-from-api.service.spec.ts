import { TestBed } from '@angular/core/testing';

import { FetchFromApiService } from './fetch-from-api.service';

describe('FetchFromApiService', () => {
  let service: FetchFromApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FetchFromApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
