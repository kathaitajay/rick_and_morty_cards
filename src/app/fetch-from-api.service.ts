import { Injectable } from '@angular/core';
import axios from 'axios';
import { BehaviorSubject } from 'rxjs';
import { NgIf } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class FetchFromApiService {
  constructor(){}
  filters={};
  /**params
   * @url full url of the api to call.
   * @method  sending method to use.
   * @payload payload to send if there is any default: null
   */
  call(url,method,payload=null){
    return new Promise((resolve,reject)=>{
      if(payload){
        axios({
          method: method,
          url: url,
          data:payload
        }).then((response)=>{
          resolve(response.data);
        }).catch((err)=>{
          reject(err);
        });
      }else{
        axios({
          method: method,
          url: url,
        }).then((response)=>{
          resolve(response.data);
        }).catch((err)=>{
          reject(err);
        });
      }
      });

    }
  setFilter(filters){
    this.filters=filters;
    return true;
    }
  
  getFilter(filterName=null){
    if(!filterName){
      return this.filters;
    }
    return this.filters[filterName];
  }
  callWithFilters(url,method,filters,payload=null){
    //{name:rick}
      let baseUrl=url+'?';
      let filtersArray=[];
      for(let key in filters){
        filtersArray.push(key+'='+filters[key]);
      }
      console.log(baseUrl+filtersArray.join('&'),"call with filt")
      if(filtersArray.length>0){
        return this.call(baseUrl+filtersArray.join('&'),method,payload);
      }
      else{
        return this.call(url,method,payload);
      }
    }
}
