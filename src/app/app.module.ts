import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injectable } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AboutComponent } from './about/about.component';
import { HeaderComponent } from './header/header.component';
import { CharacterCardComponent } from './character-card/character-card.component';
import { AppcompComponent } from './appcomp/appcomp.component';
import { WllComponent } from './wll/wll.component'
import { FormsModule } from '@angular/forms'; 

@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    HeaderComponent,
    CharacterCardComponent,
    AppcompComponent,
    WllComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
