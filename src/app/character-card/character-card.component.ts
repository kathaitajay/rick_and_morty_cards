import { Component, OnInit, Input } from '@angular/core';
import { FetchFromApiService } from '../fetch-from-api.service';

@Component({
  selector: 'app-character-card',
  templateUrl: './character-card.component.html',
  styleUrls: ['./character-card.component.css']
})
export class CharacterCardComponent implements OnInit {
  @Input() data: any;
  character:any={};
  episodes:any=[];
  isDataAvailable:boolean=false;
  constructor(private fetch:FetchFromApiService) {
    console.log('character id,data',this.data);
    if(this.data){
      this.character=this.data;
      this.episodes=this.character.episode;
      this.isDataAvailable=true;
    }
   }

  ngOnInit(): void {
    if(this.data===null){
    this.fetch.call("https://rickandmortyapi.com/api/character/"+String(this.data),'get').then((result)=>{
      console.log("result", result);
      this.character=result;
      this.episodes=this.character.episode;
      this.isDataAvailable=true;
    }).catch((err)=>{
      console.log('error', err);
    });
  }
  else{
    this.character=this.data;
    this.episodes=this.character.episode;
    this.isDataAvailable=true;
  }
  }

}
