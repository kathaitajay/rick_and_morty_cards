import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WllComponent } from './wll.component';

describe('WllComponent', () => {
  let component: WllComponent;
  let fixture: ComponentFixture<WllComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WllComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
